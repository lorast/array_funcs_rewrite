<?php
include 'array_value_rewrite.php';
include 'array_map_rewrite.php';
include 'array_reduce_rewrite.php';
include 'array_filter_rewrite.php';
//some callbacks
$call = function ($value) {
    return floor($value / 2);
};

$sum = function ($carry, $value) {
    $carry = $carry * $value;
    return $carry;
};

$even = function ($value) {
    return ($value + 1 == 11);
};

// array_map
$result = [];
$array = [1, 11, 2, 12, 3, 14];

$result = array_map_rewrite($call, $array);
print_r($result);

echo "\n================================================\n";
// array_value
$array = [
    'it' => 1,
    22,
    '3'=>11,
    [1,6],
    3
];
$result = [];

$result = array_value_rewrite($array);
print_r($result);

echo "\n================================================\n";
// array_reduce
$result = [];
$array = [1, 2, 3, 4, 5];

$result = array_reduce_rewrite($array, $sum, 3);
print_r("\n".$result);
//print_r("\n".array_reduce($array, $sum, 3)."\n");

echo "\n================================================\n";
// array_filter
$result = [];
$array = [1, 4, 18, 11, 3, 21, 10];

$result = array_filter_rewrite($array, $even);
print_r($result);