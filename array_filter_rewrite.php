<?php
/**
 * Created by PhpStorm.
 * User: dmitriy
 * Date: 10/12/17
 * Time: 12:26 PM
 */

function array_filter_rewrite (array $array, callable $callback) : array
{
    $filtered = [];
    if (is_callable($callback)) {
        foreach ($array as $value) {
            if (call_user_func($callback, $value)) {
                array_push($filtered, $value);
            }
        }
    } else {
        throw new Exception("Not callable!");
    }

    return $filtered;
}