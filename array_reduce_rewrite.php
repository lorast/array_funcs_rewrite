<?php
/**
 * Created by PhpStorm.
 * User: dmitriy
 * Date: 10/12/17
 * Time: 11:22 AM
 */

function array_reduce_rewrite (array $array, callable $callback, $initial = 0) : string
{
    $result = "";
    $prev = "";

    if (is_callable($callback)) {
        $prev = $initial;
        for ($i = 0; $i < count($array); $i++) {
            $prev = call_user_func($callback, $prev, $array[$i]);

            $result = $prev;
        }
    } else {
        throw new Exception("Not Callable!");
    }

    return $result;
}