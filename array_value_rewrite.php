<?php
/**
 * Created by PhpStorm.
 * User: dmitriy
 * Date: 10/12/17
 * Time: 10:20 AM
 */

/**
 * @param array $array
 * @return array
 */
function array_value_rewrite (array $array) : array
{
    $newarray = [];
    $counter = 0;

    foreach ($array as $key => $value) {
        $newarray[$counter] = $array[$key];

        $counter++;
    }

    return $newarray;
}