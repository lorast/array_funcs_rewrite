<?php
/**
 * Created by PhpStorm.
 * User: dmitriy
 * Date: 10/12/17
 * Time: 9:40 AM
 */

/**
 * @param callable $callback
 * @param array $array
 * @return array
 * @throws Exception
 */
function array_map_rewrite (callable $callback, array $array) : array
{
    if (is_callable($callback)) {
        foreach ($array as $key => $value) {
            $value = call_user_func($callback, $value);
            $array[$key] = $value;
        }
    } else {
        throw new Exception("Not callable!");
    }

    return $array;
}